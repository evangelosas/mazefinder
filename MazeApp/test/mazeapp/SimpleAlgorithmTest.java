package mazeapp;

import java.util.Collection;
import java.util.Iterator;
import mazeapp.core.Board;
import mazeapp.core.Square;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class SimpleAlgorithmTest {
    
    @Test
    public void testSolveTrivialCase() {
        System.out.println("solve trivial case");
        Board b = new Board(1,1);
        b.setSquare(0,0,false);
        SimpleAlgorithm instance = new SimpleAlgorithm(b,0,0,0,0);
        Path result = instance.solve();
        Collection<Square> squares = result.getSquares();
        assertEquals(1, squares.size());
    }
    
    @Test
    public void testSolveSimpleCase() {
        System.out.println("solve simple case 1 column");
        Board b = new Board(1,3);
        b.setSquare(0,0,false);
        b.setSquare(0,1,false);
        b.setSquare(0,2,false);
        SimpleAlgorithm instance = new SimpleAlgorithm(b,0,0,0,2);
        Path result = instance.solve();
        Collection<Square> squares = result.getSquares();
        assertEquals(3, squares.size());
        assertEquals(true,squares.contains(b.getSquare(0, 1)));
    }
    
    @Test
    public void testSolveSimpleCase2() {
        System.out.println("solve simple case 2 row");
        Board b = new Board(3,1);
        b.setSquare(0,0,false);
        b.setSquare(1,0,false);
        b.setSquare(2,0,false);
        SimpleAlgorithm instance = new SimpleAlgorithm(b,2,0,0,0);
        Path result = instance.solve();
        Collection<Square> squares = result.getSquares();
        assertEquals(3, squares.size());
        assertEquals(true,squares.contains(b.getSquare(1, 0)));
    }
    
    @Test
    public void testSolveEmpty() {
        System.out.println("solve empty case");
        Board b = new Board(4,3);
        for(int i = 0; i < 4; ++i)
        {
            for(int j = 0; j < 3; ++j)
            {
                b.setSquare(i,j,false);
            }
        }
        SimpleAlgorithm instance = new SimpleAlgorithm(b,0,0,0,1);
        Path result = instance.solve();
        Collection<Square> squares = result.getSquares();
        assertEquals(12, squares.size());
        Iterator<Square> it = squares.iterator();
        assertEquals(it.next(),b.getSquare(0, 0));
        assertEquals(it.next(),b.getSquare(1, 0));
        assertEquals(it.next(),b.getSquare(2, 0));
        assertEquals(it.next(),b.getSquare(3, 0));
        assertEquals(it.next(),b.getSquare(3, 1));
        assertEquals(it.next(),b.getSquare(3, 2));
        assertEquals(it.next(),b.getSquare(2, 2));
        assertEquals(it.next(),b.getSquare(2, 1));
        assertEquals(it.next(),b.getSquare(1, 1));
        assertEquals(it.next(),b.getSquare(1, 2));
        assertEquals(it.next(),b.getSquare(0, 2));
        assertEquals(it.next(),b.getSquare(0, 1));
    }
   
}
