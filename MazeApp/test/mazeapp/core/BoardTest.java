package mazeapp.core;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class BoardTest {
    
    Board b;
    
    /**
     *  0#0#
     *  0000
     *  0000
     */
    public BoardTest() {
        b = new Board(3,4);
    }
    
    /**
     * Test of getRows method, of class Board.
     */
    @Test
    public void testGetRows() {
        System.out.println("getRows");
        assertEquals(3, b.getRows());
        Board b1 = new Board(0,0);
        assertEquals(1, b1.getRows());
    }

    /**
     * Test of getColumns method, of class Board.
     */
    @Test
    public void testGetColumns() {
        System.out.println("getColumns");
        assertEquals(4, b.getColumns());
        Board b1 = new Board(-1,-1);
        assertEquals(1, b1.getRows());
    }
    
    /**
     * Test of getAdjacentSquares method, of class Board.
     */
    @Test
    public void testGetAdjacentSquares() {
         for(int i = 0; i < b.getRows(); ++i)
        {
            for(int j = 0; j < b.getColumns(); ++j)
            {
                b.setSquare(i, j, false);
            }
        }
        b.setSquare(0, 1, true);
        b.setSquare(0, 3, true);
        assertEquals(false, b.getSquare(1, 0).isWall());
        assertEquals(true, b.getSquare(0, 1).isWall());
        System.out.println("getAdjacentSquares");
        List<Square> list1 = b.getAdjacentSquares(b.getSquare(1, 2));
        assertEquals(4, list1.size());
        Square s11 = list1.get(1);
        boolean result11 = s11.equals(b.getSquare(1, 3));
        assertEquals(true, result11);
        Square s12 = list1.get(2);
        boolean result12 = s12.equals(b.getSquare(2, 2));
        assertEquals(true, result12);
        List<Square> list2 = b.getAdjacentSquares(b.getSquare(0, 0));
        Square s2 = list2.get(0);
        boolean result2 = s2.equals(b.getSquare(1, 0));
        assertEquals(true, result2);
    }
    
}
