package mazeapp.core;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class SquareTest {
    
    /**
     * Test of equals method, of class Square.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Square a1 = new Square(1,1,false);
        Square a2 = a1;
        Square a3 = new Square(1,1,true);
        Square b = new Square(1,2,true);
        Square c = new Square(2,1,true);
        boolean result1 = a1.equals(a2);
        assertEquals(true, result1);
        boolean result2 = a1.equals(a3);
        assertEquals(true, result2);
        boolean result3 = a1.equals(b);
        assertEquals(false, result3);
        boolean result4 = a1.equals(c);
        assertEquals(false, result4);
    }
    
}
