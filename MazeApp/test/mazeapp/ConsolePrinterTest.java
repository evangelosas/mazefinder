package mazeapp;

import mazeapp.core.Board;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class ConsolePrinterTest {

    /**
     * Test of print method, of class ConsolePrinter.
     */
    @Test
    public void testPrint() {
        Board b = new Board(1, 5);
        b.setSquare(0, 0, false);
        b.setSquare(0, 1, false);
        b.setSquare(0, 2, false);
        b.setSquare(0, 3, true);
        b.setSquare(0, 4, false);
        Path path = new SimplePath();
        path.addSquare(b.getSquare(0, 0));
        path.addSquare(b.getSquare(0, 1));
        path.addSquare(b.getSquare(0, 2));
        ConsolePrinter instance = new ConsolePrinter(b);
        instance.print(path);
        assertEquals('S',instance.getBoardOutput()[0][0]);
        assertEquals('X',instance.getBoardOutput()[0][1]);
        assertEquals('E',instance.getBoardOutput()[0][2]);
        assertEquals('#',instance.getBoardOutput()[0][3]);
        assertEquals(' ',instance.getBoardOutput()[0][4]);
    }


}
