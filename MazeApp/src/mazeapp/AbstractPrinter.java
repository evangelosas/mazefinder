package mazeapp;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public interface AbstractPrinter {
    
    public void print(Path path);
    
}
