package mazeapp;

import java.util.List;
import java.util.Stack;
import mazeapp.core.Board;
import mazeapp.core.Square;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class SimpleAlgorithm extends Algorithm {

    protected class Edge {

        protected Square previousSquare;
        protected Square currentSquare;

        protected Edge(Square currentSquare, Square previousSquare) {
            this.previousSquare = previousSquare;
            this.currentSquare = currentSquare;
        }

        protected Edge(Square currentSquare) {
            this.currentSquare = currentSquare;
            previousSquare = null;
        }
    }

    protected Stack<Edge> stackOfEdges;
    protected List<Square> path;
    //This keeps track to avoid visiting squares already visited.
    protected boolean[][] visitedSquares;

    public SimpleAlgorithm(Board b, int startRow, int startColumn, int endRow, int endColumn) {
        super(b, b.getSquare(startRow, startColumn), b.getSquare(endRow, endColumn));
        stackOfEdges = new Stack<>();
        stackOfEdges.add(new Edge(startingPosition));
        visitedSquares = new boolean[b.getRows()][b.getColumns()];
        for (boolean[] row : visitedSquares) {
            for (boolean s : row) {
                s = false;
            }
        }
    }

    @Override
    /**
     * This is an implementation of a non recursive DFS
     */
    public Path solve() {
        Path solution = new SimplePath();
        while (!stackOfEdges.isEmpty()) {
            Edge currentEdge = stackOfEdges.pop();
            visitedSquares[currentEdge.currentSquare.getRow()][currentEdge.currentSquare.getColumn()] = true;
            while (!solution.isEmpty() && !solution.getCurrentSquare().equals(currentEdge.previousSquare)) {
                solution.removeLastSquare();
            }
            solution.addSquare(currentEdge.currentSquare);

            if (currentEdge.currentSquare.equals(endingPosition)) {
                break;
            }

            List<Square> adjacentSqaures = board.getAdjacentSquares(currentEdge.currentSquare);
            adjacentSqaures.stream().filter((s) -> 
                    (!s.equals(currentEdge.previousSquare) && !visitedSquares[s.getRow()][s.getColumn()])).forEach((Square s) -> {
                        stackOfEdges.add(new Edge(s, currentEdge.currentSquare));
            });
        }
        
        //In case there is no solution, add the endingPosition at the end
        if (!solution.getCurrentSquare().equals(endingPosition))
        {
            solution.addSquare(endingPosition);
        }
        return solution;
    }

}
