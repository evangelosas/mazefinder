/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazeapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.stream.Stream;
import mazeapp.core.Board;

public final class ConsoleReader implements AbstractReader {

    final class IntegerPair {
        
        int FIRST,SECOND;
        
        IntegerPair(String line) {
            StringTokenizer st = new StringTokenizer(line);
            FIRST = Integer.parseInt(st.nextToken());
            SECOND = Integer.parseInt(st.nextToken());
        }
    }
    
    private String filename;

    public ConsoleReader() {
        filename = initialiseFileName();
    }

    private String initialiseFileName() {
        System.out.println("Please give the file name");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            return br.readLine();
        } catch (IOException ex) {
            return "";
        }
    }

    @Override
    public Algorithm read() {
        try (Stream<String> stream = Files.lines(Paths.get(filename))) {
            return processFileStream(stream);
        } catch (IOException e) {
            return new EmptyAlgorithm();
        }
        
    }
    
    private SimpleAlgorithm processFileStream(Stream<String> stream)
    {
        Iterator<String> it = stream.iterator();
        IntegerPair boardProperties = new IntegerPair(it.next());
        Board board = new Board(boardProperties.SECOND,boardProperties.FIRST);
        IntegerPair startPosition = new IntegerPair(it.next());
        IntegerPair endPosition = new IntegerPair(it.next());
        for(int row = 0; row < boardProperties.SECOND; ++row)
        {
            StringTokenizer st = new StringTokenizer(it.next());
            for(int column = 0; column < boardProperties.FIRST; ++column)
            {
                board.setSquare(row, column, isWall(st.nextToken()));
            }
        }
        return new SimpleAlgorithm(
                board,
                startPosition.SECOND,startPosition.FIRST,
                endPosition.SECOND,endPosition.FIRST
        );
    }
    
    private boolean isWall(String nextToken) {
        return nextToken.equals("1");
    }
}
