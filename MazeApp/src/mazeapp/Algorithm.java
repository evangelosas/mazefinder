package mazeapp;

import mazeapp.core.Board;
import mazeapp.core.Square;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public abstract class Algorithm{
    
    protected Square startingPosition;
    protected Square endingPosition;
    protected Board board;

    protected Algorithm( Board board, Square startingPosition, Square endingPosition) {
        this.startingPosition = startingPosition;
        this.endingPosition = endingPosition;
        this.board = board;
    }
    
    public Board getBoard() {
        return board;
    }
    
    public abstract Path solve();
}
