package mazeapp;

import mazeapp.core.Board;
import mazeapp.core.Square;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class ConsolePrinter implements AbstractPrinter {

    private final char WALL = '#';
    private final char EMPTY = ' ';
    private final char START = 'S';    
    private final char PATH = 'X';
    private final char END = 'E';

    
    private final char[][] boardOutput;

    public ConsolePrinter(Board b) {
        if (null == b) {
            boardOutput = new char[1][1];
            boardOutput[0][0] = WALL;
        } else {
            boardOutput = new char[b.getRows()][b.getColumns()];
            for (int row = 0; row < boardOutput.length; ++row) {
                for (int column = 0; column < boardOutput[0].length; ++column) {
                     boardOutput[row][column] = b.getSquare(row, column).isWall() ? WALL : EMPTY;
                }
            }
        }
    }

    @Override
    public void print(Path path) {

        if (null == path) {
            System.out.println("No solution found! The algorithm did not work as expected.");
            return;
        }
        
        Square[] squares = path.getSquares().toArray(new Square[path.getSquares().size()]);
        if (0 == squares.length) {
            System.out.println("No solution found! Check the filename you provided");
            return;
        }
        
        boardOutput[squares[0].getRow()][squares[0].getColumn()] = START;
        int lastSquare = squares.length - 1;
        boardOutput[squares[lastSquare].getRow()][squares[lastSquare].getColumn()] = END;
        for(int i = 1; i < lastSquare; ++i)
        {
            boardOutput[squares[i].getRow()][squares[i].getColumn()] = PATH;
        }
        outputBoard();
    }

    private void outputBoard() {
        for (char[] row : boardOutput) {
            for (char c : row) {
                System.out.print(c);
            }
            System.out.println();
        }
    }

    // For the unit testing only
    public char[][] getBoardOutput() {
        return boardOutput;
    }
    
    

}
