package mazeapp.core;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class Board {

    private final Square[][] board;

    public Board(int rows, int columns) {
        if (rows < 1) rows = 1;
        if (columns < 1) columns = 1;
        board = new Square[rows][columns];
    }

    public void setSquare(int r, int c, boolean value) {
        try {
            board[r][c] = new Square(r, c, value);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    public Square getSquare(int r, int c) {
        try {
            return board[r][c];
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
    
    public int getRows()
    {
        return board.length;
    }
    
    public int getColumns()
    {
        return board[0].length;
    }

    /**
     * Starting from the Square one row above and moving clockwise add all the
     * adjacent Squares that are not walls
     *
     * @param s
     * @return The list of adjacent
     */
    public List<Square> getAdjacentSquares(Square s) {
        List<Square> neighbours = new ArrayList<>();
        int row = s.getRow();
        int column = s.getColumn();
        if (getRows() > 1 && row != 0 && !board[row - 1][column].isWall() ) {
            neighbours.add(board[row - 1][column]);
        }
        if (getColumns() > 1 && column != board[0].length - 1 && !board[row][column + 1].isWall()) {
            neighbours.add(board[row][column + 1]);
        }
        if (getRows() > 1 && row != board.length - 1 && !board[row + 1][column].isWall()) {
            neighbours.add(board[row + 1][column]);
        }
        if (getColumns() > 1 && column != 0 && !board[row][column - 1].isWall()) {
            neighbours.add(board[row][column - 1]);
        }
        return neighbours;
    }
}
