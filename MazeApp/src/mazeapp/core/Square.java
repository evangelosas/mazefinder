package mazeapp.core;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class Square {
    
    private final int row;
    private final int column;
    private boolean wall;
    
    public Square(int row, int column, boolean wall) {
        this.row = row;
        this.column = column;
        this.wall = wall;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isWall() {
        return wall;
    }

    @Override
    //Only as a good coding practice
    public int hashCode() {
        int hash = row * 65535 + column;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Square other = (Square) obj;
        if (this.row != other.row) {
            return false;
        }
        if (this.column != other.column) {
            return false;
        }
        return true;
    }
    
}
