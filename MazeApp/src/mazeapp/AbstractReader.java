package mazeapp;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public interface AbstractReader {
    
    public Algorithm read();
    
}
