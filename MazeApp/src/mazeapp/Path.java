package mazeapp;


import java.util.Collection;
import mazeapp.core.Square;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public interface Path {
    
    public void addSquare(Square sq);
    public void removeLastSquare();
    public Collection<Square> getSquares();
    public boolean isEmpty();
    public Square getCurrentSquare();
}
