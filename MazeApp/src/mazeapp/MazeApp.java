package mazeapp;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class MazeApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractReader reader = new ConsoleReader();
        Algorithm algorithm = reader.read();
        Path path = algorithm.solve();
        AbstractPrinter printer = new ConsolePrinter(algorithm.getBoard());
        printer.print(path);
    }
    
}
