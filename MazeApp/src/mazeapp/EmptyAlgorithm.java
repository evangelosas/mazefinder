package mazeapp;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class EmptyAlgorithm extends Algorithm{

    public EmptyAlgorithm() {
        super(null, null, null);
    }

    @Override
    public Path solve() {
        return new SimplePath();
    }
}
