package mazeapp;

import java.util.Collection;
import java.util.Stack;
import mazeapp.core.Square;

/**
 *
 * @author Evangelos Asimakopoulos
 */
public class SimplePath implements Path{
    
    private Stack<Square> solutionPath;

    public SimplePath() {
        solutionPath = new Stack<>();
    }
    
    @Override
    public void addSquare(Square sq)
    {
        solutionPath.add(sq);
    }
    
    @Override
    public Collection<Square> getSquares()
    {
        return solutionPath;
    }

    @Override
    public boolean isEmpty() {
       return solutionPath.isEmpty();
    }

    @Override
    public void removeLastSquare() {
        solutionPath.pop();
    }

    @Override
    public Square getCurrentSquare() {
        return solutionPath.peek();
    }
    
}
